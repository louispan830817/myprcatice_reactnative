/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    TextInput,
    Image,
    StatusBar,
    Dimensions,
    TouchableOpacity
} from 'react-native';
const width = Dimensions.get('window').width;

export default class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Account: '',
            Password: '',
        };
    }
    componentDidMount() {
    }
    confirm = () => {
        if (this.state.Account === 'admin' && this.state.Password === 'admin') {
            alert('true')

        } else {
            alert('false')
        }
    };
    render() {

        return (
            <View style={styles.contain}>
                <Image
                    source={require}
                />
                <Text style={styles.texttheme}>我的衣櫃間</Text>
                <TextInput
                    style={styles.textInput}
                    placeholder='Account'
                    onChangeText={text => { this.setState({ Account: text }); }}
                    value={this.state.Account}
                />
                <TextInput
                    style={styles.textInput}
                    placeholder='Password'
                    onChangeText={text => { this.setState({ Password: text }); }}
                    value={this.state.Password}
                />
                <TouchableOpacity
                    onPress={() => this.confirm()}>
                    <Text style={styles.button}>登入</Text>
                </TouchableOpacity>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    contain: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 180,
        paddingLeft: 64,
        paddingRight: 64
    },
    texttheme: {
        color: 'gray',
        fontSize: 30,
        fontWeight: 'bold',
        textShadowColor: 'darkgray',
        textShadowOffset: { width: 6, height: 5 },
        textShadowRadius: 10,
        marginBottom: 15,
    },
    textInput: {
        padding: 1,
        borderBottomWidth: 1,
        borderBottomColor: 'darkgray',
        borderEndWidth: width - 128,
        marginBottom: 15
    },
    button: {
        fontSize: 16,
        color: 'cornflowerblue',
        marginTop: 10
    }
});
